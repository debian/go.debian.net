FROM debian:latest

ARG USERNAME=godebian
ARG USER_UID=1000
ARG USER_GID=1000

ENV FORWARDED_ALLOW_IPS="127.0.0.1"
RUN \
    --mount=type=cache,target=/var/cache/apt \
    apt update && \
    apt -y dist-upgrade && \
    apt -y install \
        curl \
        gunicorn \
        python3-ipy \
        python3-memcache \
        python3-pip \
        python3-psycopg2 \
        python3-venv \
        sqlite3

# Create the user
RUN groupadd --gid $USER_GID $USERNAME \
    && useradd --uid $USER_UID --gid $USER_GID -M -d /srv/godebian $USERNAME

RUN mkdir -p /srv/godebian/instance

COPY requirements.txt /srv/godebian/requirements.txt
RUN chown -R $USERNAME: /srv/godebian

WORKDIR /srv/godebian
USER $USERNAME
RUN cp /etc/skel/.bashrc .
RUN echo 'source /srv/godebian/venv/bin/activate' >> .bashrc
RUN python3 -mvenv --system-site-packages venv
RUN venv/bin/pip3 install -r requirements.txt
COPY --chown=$USERNAME:$USERNAME godebian /srv/godebian/godebian
COPY --chown=$USERNAME:$USERNAME run_gunicorn.sh /srv/godebian/run_gunicorn.sh
RUN chmod 755 /srv/godebian/run_gunicorn.sh

RUN venv/bin/flask --app godebian.web godebian init_db

EXPOSE 8009
HEALTHCHECK --interval=1m --timeout=2s \
  CMD curl -f http://localhost:8009/metrics || exit 1

CMD [ "/srv/godebian/run_gunicorn.sh"]
