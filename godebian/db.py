# -*- coding: utf-8 -*-
__author__ = "Bernd Zeimetz"
__contact__ = "bzed@debian.org"
__license__ = """
Copyright (C) 2010-2024 Bernd Zeimetz <bzed@debian.org>

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:

   1. Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
   2. Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided
      with the distribution.

THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""

import datetime
import random

from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.orm import DeclarativeBase, Mapped, mapped_column
from sqlalchemy import and_, not_
from sqlalchemy.exc import DataError
from flask import current_app

URL_ID_SEQ = "url_id_seq"
URL_TABLE = "godebian_urls"


class Base(DeclarativeBase):
    pass


db = SQLAlchemy(model_class=Base)


def init_app(app):
    db.init_app(app)


def create_all(*args, **kwargs):
    db.create_all(*args, **kwargs)


class Url(db.Model):
    __tablename__ = URL_TABLE
    pk: Mapped[int] = mapped_column(
        db.BigInteger().with_variant(db.Integer, "sqlite"), primary_key=True
    )
    id: Mapped[int] = mapped_column(  # noqa: A003
        db.BigInteger().with_variant(db.Integer, "sqlite"),
        db.Sequence(URL_ID_SEQ),
        index=True,
        unique=True,
        nullable=False,
    )
    url: Mapped[str] = mapped_column(db.String, index=True, unique=False, nullable=True)
    is_static: Mapped[bool] = mapped_column(
        db.Boolean, index=False, unique=False, nullable=False, default=False
    )
    create_date: Mapped[datetime.datetime] = mapped_column(
        db.DateTime(timezone=True), server_default=db.func.now()
    )
    log: Mapped[str] = mapped_column(db.Text, nullable=True)


class DbException(Exception):
    def __init__(self, url_id, url):
        self.id = url_id
        self.url = url


class DbIdNotStaticException(DbException):
    def __str__(self):
        return "Id %s is not static, can't update." % (str(self.id),)


class DbIdNotExistsException(DbException):
    def __str__(self):
        return "Id %s does not exist in Database" % (str(self.id),)


class DbIdExistsException(DbException):
    def __str__(self):
        return "Id %s exists in Database" % (str(self.id),)


class DbIdOutOfRangeException(DbException):
    def __str__(self):
        return "Id %s is too large to be inserted into the database" % (str(self.id),)


def _abort_session():
    db.session.rollback()
    db.session.close()


def get_url(url_id):
    url = db.session.execute(db.select(Url.url).where(Url.id == url_id)).one_or_none()
    if url:
        return url[0]
    return None


def add_url(url, static_id=None, log=None):
    if "sqlite://" in current_app.config["SQLALCHEMY_DATABASE_URI"]:
        use_sqlite = True
    else:
        use_sqlite = False

    def _add_url_to_session(url, new_id, is_static=False, log=None):
        new_urlobj = Url(url=url, id=new_id, is_static=is_static, log=log)
        db.session.add(new_urlobj)
        db.session.flush()
        return new_urlobj

    if not static_id:
        """Check if the requested URL is in the database already,
        if so return the id of the existing entry. If not,
        find the next unused id.
        """
        id_query = db.session.execute(
            db.select(Url.id).where(and_(Url.url == url, not_(Url.is_static)))
        ).one_or_none()
        if id_query:
            _abort_session()
            return id_query[0]

        if use_sqlite:
            # this is probably not perfectly safe. Don't use sqlite in production.
            rnd_id = random.randint(-1000000000, -1)
            new_urlobj = _add_url_to_session(url, rnd_id, False, log)
            new_urlobj.id = new_urlobj.pk
            new_id = new_urlobj.id
            db.session.flush()
        else:
            db.session.execute(
                db.text("""LOCK TABLE %s in SHARE MODE""" % (URL_TABLE,))
            )
            new_id = db.session.execute(
                db.text("""select nextval('%s');""" % (URL_ID_SEQ,))
            ).one()[0]
            while db.session.query(Url.id).where(Url.id == new_id)[:1]:
                new_id = db.session.execute(
                    db.text("""select nextval('%s');""" % (URL_ID_SEQ,))
                ).one()[0]
            new_urlobj = _add_url_to_session(url, new_id, False, log)
    else:
        """A static id was requested. In case the id is used already, an
        DbIdExistsException is raised."""
        new_id = static_id
        url_obj = get_url(new_id)
        if url_obj:
            raise DbIdExistsException(new_id, url)
        try:
            _add_url_to_session(url, new_id, True, log)
        except DataError:
            _abort_session()
            raise DbIdOutOfRangeException(new_id, url)
    db.session.commit()
    db.session.close()
    return new_id


def update_url(url, static_id, log=None):
    url_obj = db.session.scalars(db.select(Url).where(Url.id == static_id)).one_or_none()
    if not url_obj:
        raise (DbIdNotExistsException(static_id, url))
    if not url_obj.is_static:
        raise (DbIdNotStaticException(static_id, url))
    url_obj.url = url
    db.session.flush()
    db.session.commit()
    db.session.close()
    return static_id


def count(is_static=False):
    count = db.session.execute(
        db.select(db.func.count(Url.url)).where(Url.is_static == is_static)
    ).one()
    return count[0]
