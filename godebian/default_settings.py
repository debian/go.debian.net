import os

SQLALCHEMY_DATABASE_URI = "sqlite:///godebian.db"

URLENCODER_BLOCKSIZE = 25
URLENCODER_ALPHABET = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"

RPC_ALLOWED_IPS = "127.0.0.1, ::1, 10.0.0.0/8, 172.16.0.0/12, 192.168.0.0/16"

CONFIG_FILE = os.path.realpath(
    os.path.join(os.path.dirname(__file__), "..", "instance", "godebian.json")
)

TEMPLATE_DIR = os.path.realpath(os.path.join(os.path.dirname(__file__), "web", "views"))
STATIC_DIR = os.path.realpath(os.path.join(os.path.dirname(__file__), "web", "static"))
DOMAIN = "deb.li"
GOOGLE_SITE_VERIFICATION = ""

REAL_IP_HEADER = None

CACHE_TYPE = "SimpleCache"
CACHE_DEFAULT_TIMEOUT = 3000
