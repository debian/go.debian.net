# -*- coding: utf-8 -*-
__author__ = "Bernd Zeimetz"
__contact__ = "bzed@debian.org"
__license__ = """
Copyright (C) 2010-2024 Bernd Zeimetz <bzed@debian.org>

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:

   1. Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
   2. Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided
      with the distribution.

THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""

import IPy
import flask
import json
import logging
import os
import time
import traceback
import werkzeug
import importlib

from godebian import manage
from healthcheck import HealthCheck
from jsonrpcserver import Success, dispatch, method
from prometheus_client import Gauge
from prometheus_flask_exporter import PrometheusMetrics
from jsonschema.validators import validator_for
from markupsafe import escape

web_directory = os.path.abspath(os.path.dirname(__file__))
app = flask.Flask(
    "debian url shortener",
    template_folder=os.path.join(web_directory, "templates"),
    static_folder=os.path.join(web_directory, "static"),
    instance_path=os.path.abspath(os.path.join(web_directory, "..", "..", "instance")),
)
health = HealthCheck()
__all__ = ["app", "application"]

app.config.from_object("godebian.default_settings")
if os.path.exists(app.config["CONFIG_FILE"]):
    app.logger.info("Loading config from file: %s", app.config["CONFIG_FILE"])
    app.config.from_file(app.config["CONFIG_FILE"], load=json.load)
app.config.from_prefixed_env(prefix="GODEBIAN")

if isinstance(app.config["RPC_ALLOWED_IPS"], str):
    app.config["RPC_ALLOWED_IPS"] = app.config["RPC_ALLOWED_IPS"].split(",")
app.config["RPC_ALLOWED_IPS"] = [
    IPy.IP(x.strip()) for x in app.config["RPC_ALLOWED_IPS"]
]

logging.getLogger("jsonrpcserver").addHandler(flask.logging.default_handler)
logging.getLogger("jsonrpcserver").setLevel(logging.DEBUG)

manage.init_app(app)
metrics = PrometheusMetrics(app, group_by="endpoint")
metrics.info("app_info", "debian url shortener")

schema = json.loads(importlib.resources.read_text(__package__, "godebian-schema.json"))
klass = validator_for(schema)
klass.check_schema(schema)
validator = klass(schema).validate


class RegexConverter(werkzeug.routing.BaseConverter):
    """This converter accepts regular expressions to match URLs.

    Example:

        Rule('/pages/<re(regex=r"[234asd]+[a-z]*"):page>')

    :param regexp: regular expression to match. Will be passed to
                    the converter's self.regexp as it is.
    """

    def __init__(self, mapping, regex):
        werkzeug.routing.BaseConverter.__init__(self, mapping)
        self.regex = regex


app.url_map.converters["re"] = RegexConverter


class ShortUrlConverter(RegexConverter):
    """
    Subclass of RegexConverter
    Override __init__ with as per required regex string
    """

    def __init__(self, mapping):
        regex = "[" + app.config["URLENCODER_ALPHABET"] + "]+"
        RegexConverter.__init__(self, mapping, regex)


app.url_map.converters["shorturl"] = ShortUrlConverter


def _check_access(ip):
    """
    Verifies if request IP is allowed to access JSON-RPC API
    :param ip: ip_address of requesting system
    :return: True or False based on boolean logic
    """
    for allowed_ip in app.config["RPC_ALLOWED_IPS"]:
        if ip in allowed_ip:
            return True
    return False


__robots_txt = """User-agent: *
Allow: /
"""


@app.route("/robots.txt")
@manage.cache.cached(timeout=300)
def robots_txt():
    return app.response_class(__robots_txt, mimetype="text/plain")


__indexopts = {"title": "Welcome!"}
if app.config["GOOGLE_SITE_VERIFICATION"] != "":
    __indexopts["google_site_verification"] = app.config["GOOGLE_SITE_VERIFICATION"]


@app.route("/")
@manage.cache.cached(timeout=300)
def index():
    return flask.render_template("index.html", **__indexopts)


@app.route("/imprint.html")
@manage.cache.cached(timeout=300)
def imprint():
    return flask.render_template("imprint.html")


@app.route("/<shorturl:key>")
def redirect_by_key(key):
    url = manage.get_url(key)
    if url:
        # return flask.redirect(quote(url, safe="%/:=&?~#+!$,;'@()*[]"))
        return flask.redirect(url)
    else:
        return flask.abort(404)


@app.route("/p/<shorturl:key>")
def redirect_by_key_with_preview(key):
    url = manage.get_url(key)
    if not url:
        return flask.abort(404)
    return flask.render_template(
        "redirect-preview.html",
        domain=app.config["DOMAIN"],
        key=key,
        url=escape(url),
        preview=escape(werkzeug.urls.uri_to_iri(url)),
    )


@app.route('/<re(regex=r".+@.+"):msgid>')
@manage.cache.cached(timeout=3000)
def redirect_to_lists_debian_org(msgid):
    return flask.redirect(
        "https://lists.debian.org/msgid-search/%s" % (msgid,), code=301
    )


@app.route("/stats.html")
@manage.cache.cached(timeout=120)
def statistics():
    return flask.render_template(
        "statistics.html",
        static_urls=manage.count(is_static=True),
        non_static_urls=manage.count(is_static=False),
    )


def count_static_urls():
    return manage.count(is_static=True)


def count_non_static_urls():
    return manage.count(is_static=False)


static_url_count_metric = Gauge("godebian_static_urls", "Number of static urls")
static_url_count_metric.set_function(count_static_urls)
non_static_url_count_metric = Gauge(
    "godebian_non_static_urls", "Number of non static urls"
)
non_static_url_count_metric.set_function(count_non_static_urls)


def remote_address():
    if app.config["REAL_IP_HEADER"]:
        if app.config["REAL_IP_HEADER"] in flask.request.headers:
            header = flask.request.headers.getlist(app.config["REAL_IP_HEADER"])[0]
            return header.split(",")[0]
        else:
            app.logger.error(
                "Header %s missing from request, using remote_addr %s",
                app.config["REAL_IP_HEADER"],
                flask.request.remote_addr,
            )
            return flask.request.remote_addr
    return flask.request.remote_addr


@app.route("/rpc/json", methods=["POST"])
def json_rpc():
    try:
        _remote_address = remote_address()
    except Exception as e:
        app.logger.error("Failled to retrieve remote address from request: %s", e)
        app.logger.error(traceback.format_exc())
        return flask.abort(500)
    if not _check_access(_remote_address):
        return flask.abort(401)
    req = flask.request.get_data().decode()
    response = dispatch(req, validator=validator)
    return flask.Response(response, content_type="application/json")


@method
def add_url(url):
    url = werkzeug.urls.iri_to_uri(url)
    return Success(manage.add_url(url, log=remote_address()))


@method
def add_static_url(url, key):
    url = werkzeug.urls.iri_to_uri(url)
    return Success(manage.add_static_url(url, key, log=remote_address()))


@method
def update_static_url(url, key):
    url = werkzeug.urls.iri_to_uri(url)
    return Success(manage.update_static_url(url, key, log=remote_address()))


@method
def get_url(key):
    return Success(manage.get_url(key))


@app.errorhandler(werkzeug.exceptions.HTTPException)
def show_errormessage(error):
    template_data = {
        "code": error.code,
        "name": error.name,
        "description": error.get_description(flask.request.environ),
    }
    return flask.render_template("error.html", **template_data), error.code


godebian_ag = flask.cli.AppGroup("godebian")
app.cli.add_command(godebian_ag)


@godebian_ag.command("init_db")
def _init_db():
    with app.app_context():
        manage.db.create_all()


# healthcheck
def healthcheck():
    key = "health"
    url = "https://deb.li/health?time={}".format(time.time())
    log = "healthcheck"
    if not manage.get_url(key):
        manage.add_static_url(url, key, log=log)
    else:
        manage.update_static_url(url, key, log=log)
    return True, "deb.li up and running"


health.add_check(healthcheck)
app.add_url_rule("/health", "health", view_func=health.run)

metrics.register_default(
    metrics.counter(
        "godebian_requests_by_endpoint",
        "Request count by endpoint",
        labels={"endpoint": lambda: flask.request.endpoint},
    )
)

# keep at the end. just in case.
application = app
