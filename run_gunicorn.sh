#!/bin/bash
#


exec /srv/godebian/venv/bin/python3 /usr/bin/gunicorn --forwarded-allow-ips "${FORWARDED_ALLOW_IPS}" --bind 0.0.0.0:8009 --access-logfile - --pythonpath /srv/godebian godebian.web

