#!/usr/bin/env python3

from werkzeug.urls import iri_to_uri
import bs4
import requests
import sys
import string
import random
import time

URLS = [
    "https://bzed.at/",
    "https://www.debian.org/",
    "http://УПутина.ru/маленькие/яйца/",
    "https://台湾.tw/不是中国的一部分/",
    "http://www.umlaute.de/Ä/ä/Ö/ö/Ü/ü/",
    "https://www.weird.request.ch/?foo=bar&Ä=Ö&kaputt={meh!}",
    "https://www.weird.request.ch/?foo=bar%20%20%20fuzz",
]

GODEBIAN = sys.argv[1]
RPC_URL = "{}/rpc/json".format(sys.argv[1])


def request(method, *args):
    postdata = {
        "params": list(args),
        "method": method,
        "id": "jsonrpc",
        "jsonrpc": "2.0",
    }
    r = requests.post(RPC_URL, json=postdata)
    return r


for url in URLS:
    static_id = "".join(random.choices(string.ascii_uppercase + string.digits, k=5))
    ret_static_id = request("add_static_url", url, static_id).json()["result"]
    if static_id != ret_static_id:
        raise Exception(
            "Not returning a selected static id! ({}, {})".format(
                static_id, ret_static_id
            )
        )
    # add url
    short_url = request("add_url", url).json()["result"]
    # add url a second time.
    short_url_rep = request("add_url", url).json()["result"]
    if short_url != short_url_rep:
        raise Exception(
            "Not returning the same short url for a url! {} {}".format(
                short_url, short_url_rep
            )
        )
    stored_url = request("get_url", short_url).json()["result"]
    if iri_to_uri(url) != stored_url:
        raise Exception("URLs not equal: {} {} ({})".format(url, stored_url, short_url))
    req_url = "{}/{}".format(GODEBIAN, short_url)
    stored_url = requests.get(req_url, allow_redirects=False).headers["Location"]
    if iri_to_uri(url) != stored_url:
        raise Exception(
            "Redirect location does not match: {} => {} {} ({})".format(
                url, iri_to_uri(url), stored_url, req_url
            )
        )

    preview_url = "{}/p/{}".format(GODEBIAN, short_url)
    preview = requests.get(preview_url).content
    soup = bs4.BeautifulSoup(preview, "html.parser")
    link = soup.body.find("div", class_="col50").a.attrs["href"]
    if link != iri_to_uri(url):
        raise Exception(
            "Redirect location does not match: {} => {} {} ({})".format(
                url, iri_to_uri(url), link, preview_url
            )
        )

req_url = "{}/health".format(GODEBIAN)
for _ in range(0, 10):
    resp = requests.get(req_url, allow_redirects=False).json()
    if resp["status"] != "success":
        raise Exception("Health check broken: {})".format(resp))
    time.sleep(0.2)
